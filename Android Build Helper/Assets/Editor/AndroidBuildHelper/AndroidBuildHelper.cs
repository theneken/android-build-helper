﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class AndroidBuildHelper : ScriptableWizard
{
    [Header("Bunle Ids")]
    public string BundleId;
    public string BundleIdIOS;

    [Header("Keystore configuation")]
    public string KeystorePassword;
    public string KeyAliasName;
    public string KeystorePath;

    [Header("Others configurations")]
    public bool IncrementeBundleVersionCode;
    public bool ChangeBundleIOSAfterBuild;
    public string BuildFileName;


    private const string BUNDLEID_PLAYERPREFS_KEY = "BUNDLEID_PLAYERPREFS_KEY";
    private const string BUNDLEID_IOS_PLAYERPREFS_KEY = "BUNEDLEID_IOS_PLAYERPREFS_KEY";
    private const string KEYSTORE_PASSWORD_PLAYERPREFS_KEY = "KEYSTORE_PLAYERPREFS_KEY";
    private const string KEYALIASNAME_PLAYERPREFS_KEY = "KEYALIASNAME_PLAYERPREFS_KEY";
    private const string KEYSTORE_PATH_KEY = "KEYSTORE_PATH_KEY";
    private const string INCREMATE_KEY = "INCREMANTE_KEY";
    private const string BUILD_FILENAME_KEY = "BUILD_FILENAME_KEY";
    private const string CHANGE_BUNDLE_IOS_AFTER_BUILD_KEY = "CHANGE_BUNDLE_IOS_AFTER_BUILD_KEY";

    private bool _isLoaded;

    [MenuItem("Window/Delete All Player Prefs")]
    private static void DeletaAll()
    {
        PlayerPrefs.DeleteAll();
        PlayerPrefs.Save();
    }

    [MenuItem("Window/Android Build Helper")]
    private static void Build() 
    {
        DisplayWizard<AndroidBuildHelper>("Android Build Helper", "Build", "Save");
    }

    private static int GetIncrementedBundleVersion()
    {
        var currentVersion = PlayerSettings.Android.bundleVersionCode;
        return currentVersion + 1;
    }

    private  void SignBuild()
    {
        PlayerSettings.Android.keystoreName = KeystorePath;
        PlayerSettings.Android.keystorePass = KeystorePassword;
        PlayerSettings.Android.keyaliasName = KeyAliasName;
        PlayerSettings.Android.keyaliasPass = KeystorePassword;

    }

    #region WIZARD_MESSAGES
    private void OnWizardCreate()
    {
        var path = EditorUtility.SaveFolderPanel("Choose location of built game", "", "");

        PlayerSettings.bundleIdentifier = BundleId;
        if (IncrementeBundleVersionCode)
            PlayerSettings.Android.bundleVersionCode = GetIncrementedBundleVersion();

        SignBuild();
        BuildFileName = BuildFileName + "-" + PlayerSettings.bundleVersion + "-" + PlayerSettings.Android.bundleVersionCode + ".apk";
        BuildPipeline.BuildPlayer(UnityEditor.EditorBuildSettings.scenes, path +"/"+ BuildFileName, BuildTarget.Android, BuildOptions.None);


        if (ChangeBundleIOSAfterBuild)
            PlayerSettings.bundleIdentifier = BundleIdIOS;

    }

    private void OnWizardUpdate()
    {
   
        if (_isLoaded)
            return;

        LoadVariables();
        _isLoaded = true;

        if (string.IsNullOrEmpty(KeystorePath))
            KeystorePath = EditorUtility.OpenFilePanel("Select keystore", "", "keystore");

    }

    private void OnWizardOtherButton()
    {
        SaveVariables();
    }
    #endregion

    #region SAVE & LOAD
    private void LoadVariables()
    {
        BundleId = PlayerPrefs.GetString(BUNDLEID_PLAYERPREFS_KEY, "com.lukaszborun.game1");
        BundleIdIOS = PlayerPrefs.GetString(BUNDLEID_IOS_PLAYERPREFS_KEY, "com.lukaszborun.game_ios_1");

        KeystorePassword = PlayerPrefs.GetString(KEYSTORE_PASSWORD_PLAYERPREFS_KEY, "12345A");
        KeyAliasName = PlayerPrefs.GetString(KEYALIASNAME_PLAYERPREFS_KEY,"com.lukaszborun.game1");

        KeystorePath = PlayerPrefs.GetString(KEYSTORE_PATH_KEY, "");
        IncrementeBundleVersionCode = PlayerPrefs.GetInt(INCREMATE_KEY, 0) == 0 ? false : true;
        BuildFileName = PlayerPrefs.GetString(BUILD_FILENAME_KEY, "Game1");
        ChangeBundleIOSAfterBuild = PlayerPrefs.GetInt(CHANGE_BUNDLE_IOS_AFTER_BUILD_KEY, 0) == 0 ? false : true;
    }

    private  void SaveVariables()
    {
        PlayerPrefs.SetString(BUNDLEID_PLAYERPREFS_KEY, BundleId);
        PlayerPrefs.SetString(BUNDLEID_IOS_PLAYERPREFS_KEY, BundleIdIOS);
        PlayerPrefs.SetString(KEYSTORE_PASSWORD_PLAYERPREFS_KEY, KeystorePassword);
        PlayerPrefs.SetString(KEYALIASNAME_PLAYERPREFS_KEY, KeyAliasName);
        PlayerPrefs.SetString(KEYSTORE_PATH_KEY, KeystorePath);
        PlayerPrefs.SetInt(INCREMATE_KEY, IncrementeBundleVersionCode ? 1 : 0);
        PlayerPrefs.SetString(BUILD_FILENAME_KEY, BuildFileName);
        PlayerPrefs.SetInt(CHANGE_BUNDLE_IOS_AFTER_BUILD_KEY, ChangeBundleIOSAfterBuild ? 1 : 0);
        PlayerPrefs.Save();
    }
    #endregion

}
