# README #

### What is this repository for? ###

Adroid Build Helper is a Unity3D editor extension who's help manage Android build configuration:

*  Auto incremante Bundle Version COde

* Change IOS BundleID after successful build (If Your Game have another BundleID for iOS and Android) 

* Auto selectable Keystore 

* Auto sign Keystore with password


### How do I get set up? ###

Just download Asset Package from this [link](https://bitbucket.org/theneken/android-build-helper/downloads/AndroidBuildHelperV1-0.unitypackage). When package was downloaded, click on it and unpack to Your project. 

1. In Unity3D click on **Window -> Android Build Helper**.

2. If this's first time when extension was opened, a Dialog File will appear and You need to select Your Android Keystore file. 

3. Below are descriptions for configuration variables:

**Bundle Id** - This is Bundle Id for Your Android build eg. com.lukaszborun.game1

**Bundle iOS Id** - This is bundle ID for Your iOS configuration. (This solve a problem when Your game have different BundleId for iOS and Android)

**Keystore Password** - Your password for Keystore

**Key Alias Name** - Alias name for Your Keystore

**Key Store Path** - Full path to Your Keystore. This is auto filled after You selected Your Keystore at the start

**Increment Bundle Version Code** - If true, after Your clicked "Build" the new version will have increment bundle version code. 

**Change Bundle IOS After Successful Build** - If true, after You built a new version te BundleIdIOS will be set for PlayerSettings BundleId

**Build File Name** - A name for the build. Extension will attach bundle version and bundle version code to the end of the name.    


### Who do I talk to? ###

Łukasz "neken" Boruń 

lukasz.borun@gmail.com

### License ###

Attribution-NonCommercial-ShareAlike 3.0 Unported with simple explanation with the attribution clause waived. You are free to use Android Build Helper in any and all games that you make. You cannot sell Android Build Helper directly or as part of a larger game asset.